# coding: utf-8
import argparse
import math
import time

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams
from matplotlib.collections import PatchCollection
from matplotlib.patches import Circle
from mpmath import mp

rcParams['font.family'] = 'serif'
rcParams['font.serif'] = ['Times New Roman']


def spiral_points(arc=1.0, separation=1.0):
    """This code is from liborm's answer on this stackoverflow question:
    https://stackoverflow.com/questions/13894715/draw-equidistant-points-on-a-spiral
    Generate points on an Archimedes' spiral with `arc` giving the length of
    arc between two points and `separation` giving the distance between
    consecutive turnings
    - approximate arc length with circle arc at given distance
    - use a spiral equation r = b * phi
    """

    def p2c(radius, phi_angle):
        """Polar to cartesian coordinates
        """
        return radius * math.cos(phi_angle), radius * math.sin(phi_angle)

    # yield a point at origin
    yield 0, 0

    # initialize the next point in the required distance
    r = arc
    b = separation / (2 * math.pi)

    # find the first phi to satisfy distance of `arc` to the second point
    phi = float(r) / b
    while True:
        yield p2c(r, phi)
        # advance the variables
        # calculate phi that will give desired arc length at current radius
        # (approximating with circle)
        phi += float(arc) / r
        r = b * phi


def create_plot(num_points=None, colour_map_name=None, dpi=150):
    # Obtain a matplotlib.pyplot object of the required number of points, colour map and dpi
    the_plot = calculate_plot(num_points=num_points, colour_map_name=colour_map_name, dpi=dpi)

    # Save the plot as a png file
    filename = 'pi_art_{0}_{1}_{2}dpi.png'.format(num_points, colour_map_name, dpi)
    the_plot.savefig(filename, bbox_inches='tight', pad_inches=3.0, dpi=dpi)

    # Close the plot to free up memory
    the_plot.close()


def calculate_plot(num_points=None, colour_map_name=None, dpi=150):
    if num_points is None:
        num_points = 314
    if colour_map_name is None:
        colour_map_name = "viridis"

    spiral_arc = 1.0
    spiral_sep = 1.5
    dot_radius = 0.4

    min_x = 0
    max_x = 0
    min_y = 0
    max_y = 0

    mp.dps = num_points

    pi = str(mp.pi)
    pi = pi.replace('.', '').encode("utf-8")

    spiral = spiral_points(spiral_arc, spiral_sep)

    fig, ax = plt.subplots()
    fig.set_figheight(50)
    fig.set_figwidth(50)

    patches = []
    for _ in pi:
        current_coords = next(spiral)
        if current_coords[0] > max_x:
            max_x = current_coords[0]
        if current_coords[0] < min_x:
            min_x = current_coords[0]
        if current_coords[1] > max_y:
            max_y = current_coords[1]
        if current_coords[1] < min_y:
            min_y = current_coords[1]

        circle = Circle(current_coords, dot_radius)
        patches.append(circle)

    # https://matplotlib.org/api/pyplot_summary.html
    plt.set_cmap(colour_map_name)
    colours = np.frombuffer(pi, dtype='|S1').astype(float)
    p = PatchCollection(patches, edgecolor="black")
    p.set_array(np.array(colours))
    ax.add_collection(p)

    if min_x < min_y:
        min_y = min_x
    else:
        min_x = min_y

    if max_x > max_y:
        max_y = max_x
    else:
        max_x = max_y

    axes = plt.gca()
    axes.set_aspect(1.0)
    axes.set_xlim([min_x - dot_radius, max_x + dot_radius])
    axes.set_ylim([min_y - dot_radius, max_y + dot_radius])

    legend_patches = []
    legend_colours = np.arange(10)
    x_pos = 0.001
    y_pos = 0.04
    dot_radius_scaled = dot_radius / fig.get_dpi()
    colour_map = plt.get_cmap()

    for i in range(10):
        ax.add_patch(plt.Circle((x_pos + dot_radius_scaled, y_pos), dot_radius_scaled,
                                fill=True, facecolor=colour_map(i / 9.0), zorder=1000,
                                transform=fig.transFigure, figure=fig, edgecolor="black"))
        ax.annotate(legend_colours[i], xy=(5, 5), xycoords='axes fraction',
                    xytext=(x_pos + 0.015, y_pos - dot_radius_scaled + 0.001), size=36)
        y_pos = y_pos + 0.012

    text_to_annotate = u'The first {0} digits of \u03C0'.format(num_points)
    ax.annotate(u'\u03C0', xy=(5, 5), xycoords='axes fraction', xytext=(0.00, 0.975), size=200)
    ax.annotate(text_to_annotate, xy=(5, 5), xycoords='axes fraction', xytext=(0.00, 0.01), size=30)
    ax.annotate(r'David Platten 2022', xy=(5, 5), xycoords='axes fraction', xytext=(0.00, 0.00), size=30)

    legend_colours = np.arange(10)
    p_legend = PatchCollection(legend_patches)
    p_legend.set_array(np.array(legend_colours))
    ax.add_collection(p_legend)

    plt.axis('off')
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_frame_on(False)
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0, hspace=0, wspace=0)

    return plt

    # filename = 'pi_art_{0}_{1}_{2}dpi.png'.format(num_points, colour_map_name, dpi)
    # plt.savefig(filename, bbox_inches='tight', pad_inches=3.0, dpi=dpi)

    # # Close the plot to free up memory
    # plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--n_points", default=314, required=False, type=int,
                        help="The number of digits of pi to use")

    parser.add_argument("-c", "--colour_map", default="viridis", required=False, type=str,
                        help="The matplotlib colour map to use")

    parser.add_argument("-s", "--show_maps", required=False, action="store_true",
                        help="Show available colour maps")

    parser.add_argument("-a", "--all_maps", required=False, action="store_true",
                        help="Create a figure using every available colour map")

    parser.add_argument("-dpi", "--dots_per_inch", default=150, required=False, type=int,
                        help="The dots per inch of the png output file")

    args = parser.parse_args()

    if args.show_maps:
        print("Valid colour map options are: " + ", ".join(plt.colormaps()))
        exit(0)

    if args.colour_map:
        if args.colour_map not in plt.colormaps():
            print("{0} unavailable. The options are: ".format(args.colour_map) + ", ".join(plt.colormaps()))
            exit(0)

    if args.all_maps:
        for each_map in plt.colormaps():
            print("Working on {0} plot".format(each_map), end="", flush=True)
            create_plot(args.n_points, each_map, args.dots_per_inch)
            print("...done")
            time.sleep(0.5)
        exit(0)

    print("Creating plot", end="", flush=True)
    create_plot(args.n_points, args.colour_map, args.dots_per_inch)
    print("...done")
