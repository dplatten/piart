from os import listdir
from os.path import isfile, join

mypath = "examples"

all_files = [f for f in listdir(mypath) if isfile(join(mypath, f))]

# Sort list into case-insensitive alphabetical order
all_files = sorted(all_files, key=lambda s: s.casefold())

group_size = 2
groups_of_files = zip(*(iter(all_files),) * group_size)

with open('README.md', 'w') as f:
    f.write("Pi art\n"
            "======\n\n"

            "A python script to produce a graphical representation of pi in a spiral format using "
            "matplotlib. The user can specify the number of digits, the colour map and the dots per "
            "inch of the output image file. An example of each colour map is shown below, created "
            "using the `-dpi 10 -a` options and the default 314 digits.\n\n"

            "Usage\n"
            "=====\n\n"

            "```console\n"
            "python pi_art -h\n"
            "\n\n"

            "usage: pi_art.py [-h] [-n N_POINTS] [-c COLOUR_MAP] [-s] [-a] [-dpi DOTS_PER_INCH]\n\n"

            "optional arguments:\n"
            "  -h, --help            show this help message and exit\n"
            "  -n N_POINTS, --n_points N_POINTS\n"
            "                        The number of digits of pi to use\n"
            "  -c COLOUR_MAP, --colour_map COLOUR_MAP\n"
            "                        The matplotlib colour map to use\n"
            "  -s, --show_maps       Show available colour maps\n"
            "  -a, --all_maps        Create a figure using every available colour map\n"
            "  -dpi DOTS_PER_INCH, --dots_per_inch DOTS_PER_INCH\n"
            "                        The dots per inch of the png output file\n"
            "```\n\n"
            
            "Examples\n"
            "========\n\n")

    # Find the longest file name
    longest_name = len(max(all_files, key=len))

    # Find the longest string in the table
    max_string_length = 2 * longest_name - 6

    # Create a markup table for each group, with each cell max_string_length long
    for group in groups_of_files:
        first_string = group[0][11:-9]
        second_string = group[1][11:-9]
        # Make the first and second strings max_string_length
        if len(first_string) < max_string_length:
            first_string = first_string.ljust(max_string_length, " ")
        if len(second_string) < max_string_length:
            second_string = second_string.ljust(max_string_length, " ")

        f.write("| {0} | {1} |\n".format(first_string, second_string))
        f.write("|-{0}-|-{1}-|\n".format("-" * max_string_length, "-" * max_string_length))

        first_string = "![{0}](examples/{1})".format(group[0][11:-9], group[0])
        second_string = "![{0}](examples/{1})".format(group[1][11:-9], group[1])
        # Make the first and second strings max_string_length
        if len(first_string) < max_string_length:
            first_string = first_string.ljust(max_string_length, " ")
        if len(second_string) < max_string_length:
            second_string = second_string.ljust(max_string_length, " ")

        f.write("| {0} | {1} |\n\n".format(first_string, second_string))
