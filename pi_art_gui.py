# coding: utf-8
import tkinter as tk
from functools import partial

import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

from pi_art import calculate_plot


class Application(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        # Add a menu bar
        self.menu = tk.Menu(self)
        root.config(menu=self.menu)

        # Add a file menu
        self.file_menu = tk.Menu(self.menu, tearoff=0)
        self.file_menu.add_command(label="Save current figure", command=self.save_figure, accelerator="Ctrl+S")
        self.file_menu.add_command(label="Quit", command=self.quit, accelerator="Ctrl+Q")
        self.menu.add_cascade(label="File", menu=self.file_menu)

        # Add a colourmap menus
        self.colour_map_menu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label="Colour maps", menu=self.colour_map_menu)

        perceptually_uniform_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["viridis", "plasma", "inferno", "magma", "cividis"]
        for item in list_of_cmaps:
            perceptually_uniform_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Perceptually Uniform Sequential", menu=perceptually_uniform_menu)

        sequential_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["Greys", "Purples", "Blues", "Greens", "Oranges", "Reds", "YlOrBr", "YlOrRd", "OrRd", "PuRd",
                         "RdPu", "BuPu", "GnBu", "PuBu", "YlGnBu", "PuBuGn", "BuGn", "YlGn"]
        for item in list_of_cmaps:
            sequential_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Sequential", menu=sequential_menu)

        sequential2_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["binary", "gist_yarg", "gist_gray", "gray", "bone", "pink", "spring", "summer", "autumn",
                         "winter", "cool", "Wistia", "hot", "afmhot", "gist_heat", "copper"]
        for item in list_of_cmaps:
            sequential2_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Sequential (2)", menu=sequential2_menu)

        diverging_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["PiYG", "PRGn", "BrBG", "PuOr", "RdGy", "RdBu", "RdYlBu", "RdYlGn", "Spectral", "coolwarm",
                         "bwr", "seismic"]
        for item in list_of_cmaps:
            diverging_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Diverging", menu=diverging_menu)

        cyclic_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["twilight", "twilight_shifted", "hsv"]
        for item in list_of_cmaps:
            cyclic_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Cyclic", menu=cyclic_menu)

        qualitative_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["Pastel1", "Pastel2", "Paired", "Accent", "Dark2", "Set1", "Set2", "Set3", "tab10", "tab20",
                         "tab20b", "tab20c"]
        for item in list_of_cmaps:
            qualitative_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Qualitative", menu=qualitative_menu)

        miscellaneous_menu = tk.Menu(self.colour_map_menu, tearoff=0)
        list_of_cmaps = ["flag", "prism", "ocean", "gist_earth", "terrain", "gist_stern", "gnuplot", "gnuplot2",
                         "CMRmap", "cubehelix", "brg", "gist_rainbow", "rainbow", "jet", "turbo", "nipy_spectral",
                         "gist_ncar"]
        for item in list_of_cmaps:
            miscellaneous_menu.add_command(label=item, command=partial(self.change_colour_map, item))
        self.colour_map_menu.add_cascade(label="Miscellaneous", menu=miscellaneous_menu)

        # Add a number of digits menu
        self.n_digits_menu = tk.Menu(self.menu, tearoff=0)
        list_of_digit_options = [31, 314, 3141, 31415]
        for item in list_of_digit_options:
            self.n_digits_menu.add_command(label=str(item), command=partial(self.change_n_digits, item))
        self.menu.add_cascade(label="Digits", menu=self.n_digits_menu)

        self.dpi = 20
        self.n_digits = 314
        self.colour_map = "viridis"
        self.pi_canvas = None

        self.draw_figure_on_canvas()

    def draw_figure_on_canvas(self):
        pi_plot = calculate_plot(num_points=self.n_digits, colour_map_name=self.colour_map, dpi=self.dpi)
        pi_plot.figure(1).set_dpi(self.dpi)

        self.pi_canvas = FigureCanvasTkAgg(plt.gcf(), master=root)
        self.pi_canvas.get_tk_widget().grid(row=1, column=1, padx=20, pady=20)
        self.pi_canvas.draw_idle()
        self.pi_canvas.flush_events()

        pi_plot.close()

    def change_colour_map(self, new_colour_map):
        # Set the new colour map
        self.colour_map = new_colour_map

        # Remove all items from the canvas
        for item in self.pi_canvas.get_tk_widget().find_all():
            self.pi_canvas.get_tk_widget().delete(item)

        # Re-draw the figure on the canvas
        self.draw_figure_on_canvas()

    def change_n_digits(self, new_n_digits):
        # Set the new number of digits
        self.n_digits = new_n_digits

        # Remove all items from the canvas
        for item in self.pi_canvas.get_tk_widget().find_all():
            self.pi_canvas.get_tk_widget().delete(item)

        # Re-draw the figure on the canvas
        self.draw_figure_on_canvas()

    def save_figure(self):
        # Use a high dpi when saving the figure
        dpi_when_saving = 150

        # Create a plot using the higher dpi
        the_plot = calculate_plot(num_points=self.n_digits, colour_map_name=self.colour_map, dpi=dpi_when_saving)

        # Save the plot as a png file
        filename = 'pi_art_{0}_{1}_{2}dpi.png'.format(self.n_digits, self.colour_map, dpi_when_saving)
        the_plot.savefig(filename, bbox_inches='tight', pad_inches=3.0, dpi=dpi_when_saving)

        # Close the plot to free up memory
        the_plot.close()

    def quit(self):
        root.quit()
        root.destroy()
        exit()


# root window
root = tk.Tk()
root.resizable(True, True)
root.title("Pi art")

app = Application(master=root)
app.mainloop()
